#!/bin/bash
clear

# Function
debian_base (){
    echo "adding key"
    curl https://riot.im/packages/debian/repo-key.asc | sudo apt-key add -
    
    echo "updating package list"
    sudo apt update
    
    echo "finally installing the app"
    sudo apt install riot-web
}


if sudo cat /etc/os-release | grep -xqFe "VERSION_CODENAME=yakkety"; then
    echo "adding to sources list"
    echo "deb http://riot.im/packages/debian/ yakkety main" | sudo tee -a /etc/apt/sources.list > /dev/null
	debian_base

elif sudo cat /etc/os-release | grep -xqFe "VERSION_CODENAME=xenial"; then
    echo "adding to sources list"
    echo "deb http://riot.im/packages/debian/ xenial main" | sudo tee -a /etc/apt/sources.list > /dev/null
    debian_base

elif sudo cat /etc/os-release | grep -xqFe "VERSION_CODENAME=zesty"; then
    echo "ubuntu 17.04 doesn\'t support riot"

else
    echo "adding to sources list"
    echo "deb http://riot.im/packages/debian/ jessie main" | sudo tee -a /etc/apt/sources.list > /dev/null
    debian_base
fi
